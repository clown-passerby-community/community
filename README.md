# 汉服荟社区

### [小丑路人社区](https://bbs.cnpscy.com)（PC端）
![首页](./demo/pc.png)

### 仿汉服荟（移动端）
![首页](./demo/home.png)
![聊天](./demo/chat.png)
![签到](./demo/sign.png)


### Hyperf与Laravel版

<p align="center">
    <a href="https://gitee.com/clown-passerby-community/community" target="_blank">laravel-小丑路人社区</a>
</p>
<p align="center">
    <img src="https://gitee.com/clown-passerby-community/community/badge/star.svg?theme=dark" />
    <img src="https://gitee.com/clown-passerby-community/community/badge/fork.svg?theme=dark" />
    <img src="https://svg.hamm.cn/badge.svg?key=License&value=Apache-2.0&color=da4a00" />
</p>

<p align="center">
    <a href="https://gitee.com/clown-passerby-community/hyperf-community" target="_blank">Hyperf-小丑路人社区</a>
</p>
<p align="center">
    <img src="https://gitee.com/clown-passerby-community/hyperf-community/badge/star.svg?theme=dark" />
    <img src="https://gitee.com/clown-passerby-community/hyperf-community/badge/fork.svg?theme=dark" />
    <img src="https://svg.hamm.cn/badge.svg?key=License&value=Apache-2.0&color=da4a00" />
</p>

### 软件架构
软件架构说明（各自文件下有对应的安装流程介绍）
1. laravel APi
	* 编程语言：`PHP7.3+`
	* 后端框架： `Laravel8`
	* 前端Vue框架：`vue-element-admin`
	* Nodejs  v14.*
    * <font color='red'>禁止使用DB对数据进行删除</font>
      + 原因：`is_delete`的概念是自己封装的，无法兼容`DB::delete`删除逻辑，需手动调整为`DB::update(['is_delete' => 1, ……])`
      + 如需使用，两个步骤操作：
        + 请更换`App\Models\Model`的假删除引用为Laravel假删除模块
        + 且数据表需引入对应的删除字段
2. hyperf socket服务端
3. uni-app 汉服荟社区

### 他们正在使用
这些公司或软件正在使用我们的开源软件：
- [小丑路人·社区](https://bbs.cnpscy.com)
- [小丑路人·博客](https://www.cnpscy.com)

### 捐助
如果您觉得我们的开源软件对你有所帮助，请扫下方二维码打赏我们一杯咖啡。

![Alipay payment code](https://images.gitee.com/uploads/images/2020/1112/091130_811b3a6c_790553.jpeg "alipay-400.jpg")
![Wechat collection code](https://images.gitee.com/uploads/images/2020/1112/091305_2592a352_790553.jpeg "wechat-400-width(1).jpg")
 
### 联系
- 网站：
[小丑路人·社区](https://bbs.cnpscy.com)
- 邮箱:
`cnpscy@qq.com`